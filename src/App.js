import React from 'react';
import { Provider } from 'react-redux';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import MainView from './views/MainView';
import LoginView from './views/LoginView';
import EnterpriseDetailsView from './views/EnterpriseDetailsView';
import AuthLoadingView from './views/AuthLoadingView';
import store from './store';
import NavigationService from './NavigationService';
import { COLOR_PRIMARY } from './style/colors';

const AuthStack = createStackNavigator({
  Login: {
    screen: LoginView,
    navigationOptions: {
      header: null,
    }
  }
});

const AppStack = createStackNavigator(
  {
    Main: MainView,
    EnterpriseDetails: EnterpriseDetailsView
  },
  {
    initialRouteName: 'Main',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: COLOR_PRIMARY,
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  }
);

const AppContainer = createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: {
        screen: AuthLoadingView,
        navigationOptions: {
          header: null,
        }
      },
      App: AppStack,
      Auth: AuthStack,
    },
    {
      initialRouteName: 'AuthLoading'
    }
  )
);

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {

  }

  render() {
    return (
      <Provider store={store}>
        <AppContainer ref={(navigatorRef) => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
        />
      </Provider>
    );
  }
}

export default App;
