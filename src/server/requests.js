/* eslint-disable object-shorthand */
import axios from 'axios';

export const URL_DEFAULT = 'http://empresas.ioasys.com.br';

// eslint-disable-next-line import/prefer-default-export
export const postlogin = ({ email, password }) => {
  return axios({
    method: 'post',
    url: `${URL_DEFAULT}/api/v1/users/auth/sign_in`,
    data: {
      email: email,
      password: password
    }
  });
};

export const getEmpresas = () => {
  return axios({
    method: 'get',
    url: `${URL_DEFAULT}/api/v1/enterprises`
  });
};
export const getDetalhesEmpresas = (idEmpresa) => {
  return axios({
    method: 'get',
    url: `${URL_DEFAULT}/api/v1/enterprises/${idEmpresa}`
  });
};
