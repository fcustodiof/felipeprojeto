export const COLOR_PRIMARY = '#673AB7';
export const COLOR_SECUNDARY = '#512DA8';
export const COLOR_ACCENT = '#607D8B';
export const COLOR_PRIMARY_TEXT = '#212121';
export const COLOR_SECUNDARY_TEXT = '#757575';
export const COLOR_BACKGROUND = '#ebebeb';
export const COLOR_FACEBOOK = '#3b5998';
export const COLOR_LINKEDIN = '#0e76a8';
export const COLOR_TWITTER = '#00acee';
