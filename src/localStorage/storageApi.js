import AsyncStorage from '@react-native-community/async-storage';
import {
  SET_CREDENCIALS,
  GET_CREDENCIALS
} from './constants';

export const setCredencials = (credencials) => AsyncStorage.setItem(
  SET_CREDENCIALS,
  JSON.stringify(credencials)
);

export const getCredencials = () => AsyncStorage.getItem(GET_CREDENCIALS);
