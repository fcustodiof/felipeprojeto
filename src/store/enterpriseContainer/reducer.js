import {
  LOAD_ENTERPRISES,
  LOAD_ENTERPRISES_SUCCESS,
  LOAD_ENTERPRISES_ERROR
} from './constants';

const INITIAL_STATE = {
  error: '',
  loading: false,
  list: {}
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOAD_ENTERPRISES:
      return {
        ...state,
        loading: true,
        error: ''
      };
    case LOAD_ENTERPRISES_SUCCESS:
      return {
        ...state,
        loading: false,
        list: action.payload
      };
    case LOAD_ENTERPRISES_ERROR:
      return {
        ...state,
        error: action.payload,
        loading: false
      };
    default:
      return state;
  }
};
