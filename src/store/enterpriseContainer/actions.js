import {
  ASYNC_LOAD_ENTERPRISES
} from './constants';

export const loadEnterpriseList = () => ({
  type: ASYNC_LOAD_ENTERPRISES
});