import {
  takeLatest, put, takeEvery, call, all
} from 'redux-saga/effects';
import axios from 'axios';
import NavigationService from '../NavigationService';
import {
  LOGIN_USER,
  ASYNC_LOGIN_USER,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_ERROR
} from './loginContainer/constants';
import {
  LOAD_ENTERPRISES,
  LOAD_ENTERPRISES_SUCCESS,
  LOAD_ENTERPRISES_ERROR,
  ASYNC_LOAD_ENTERPRISES
} from './enterpriseContainer/constants';
import { postlogin } from '../server/requests';

function setupCredenctials(response) {
  axios.defaults.headers.common['client'] = response.headers.client;
  axios.defaults.headers.common['access-token'] = response.headers['access-token'];
  axios.defaults.headers.common['uid'] = response.headers.uid;
  axios.defaults.headers.get['Content-Type'] = 'application/json';
}

function* asyncLogin(action) {
  yield put({ type: LOGIN_USER, payload: true });
  try {
    const response = yield call(postlogin, action.payload);
    yield put({ type: LOGIN_USER_SUCCESS, payload: response.data });
    setupCredenctials(response);
    yield put(NavigationService.navigate('App'));
  } catch (err) {
    yield put({ type: LOGIN_USER_ERROR, payload: err });
  }
}

function* asyncEnterpriseList() {
  yield put({ type: LOAD_ENTERPRISES, payload: true });
  try {
    const response = yield call(postlogin);
    console.log(response);
    yield put({ type: LOAD_ENTERPRISES_SUCCESS, payload: response.data });
  } catch (err) {
    yield put({ type: LOAD_ENTERPRISES_ERROR, payload: err });
  }
}

export default function* root() {
  yield all([
    takeEvery(ASYNC_LOGIN_USER, asyncLogin),
    takeEvery(ASYNC_LOAD_ENTERPRISES, asyncEnterpriseList),
  ]);
}
