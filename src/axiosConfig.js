import axios from 'axios';

export const setupAxios = () => {
  axios.interceptors.response.use((response) => {
    console.log(response);
    return response;
  }, (error) => {
    console.log(error);
    return Promise.reject(error);
  });

  axios.interceptors.request.use((config) => {
    console.log(config);
    return config;
  }, (error) => {
    console.log(error);
    return Promise.reject(error);
  });
};

export const updateHeaderRequest = (token) => {
  axios.defaults.headers.common['Authorization'] = token;
};
