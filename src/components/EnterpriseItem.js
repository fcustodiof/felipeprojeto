/* eslint-disable camelcase */
import React from 'react';
import {
  View,
  StyleSheet,
  TouchableNativeFeedback,
  Image,
  Linking
} from 'react-native';
import {
  TextTitle, TextSimple, Row, IconButton
} from './common';
import { PADDING_CARD } from '../style/dimensions';
import { COLOR_FACEBOOK, COLOR_TWITTER, COLOR_LINKEDIN } from '../style/colors';
import { URL_DEFAULT } from '../server/requests';

const LOGO_IMAGE = require('../assets/images/logo_ioasys.png');

class EnterpriseItem extends React.Component {
  openSocial = (url) => {
    if (url) {
      Linking.getInitialURL().then((response) => {
        if (response) {
          console.log('Initial url is: ' + url);
        }
      }).catch(err => console.error('An error occurred', err));
    }
  }

  render() {
    const { onPress, item, disabled } = this.props;
    const {
      city,
      country,
      description,
      email_enterprise,
      enterprise_name,
      enterprise_type,
      facebook,
      id,
      linkedin,
      own_enterprise,
      phone,
      photo,
      share_price,
      twitter,
      value
    } = item;
    return (
      <View>
        <TouchableNativeFeedback
          disabled={disabled}
          onPress={onPress}
          background={TouchableNativeFeedback.SelectableBackground()}
        >
          <View style={styles.itemContainerStyle}>
            <Image
              progressiveRenderingEnabled
              loadingIndicatorSource={LOGO_IMAGE}
              source={photo ? { uri: URL_DEFAULT + photo } : LOGO_IMAGE}
              style={styles.imageStyle}
            />
            <TextTitle>{`Empresa: ${enterprise_name}`}</TextTitle>
            <TextSimple>{ `${city} - ${country}` }</TextSimple>
            <TextSimple style={styles.textJust}>
              { description }
            </TextSimple>
            <TextSimple>{ `Email: ${email_enterprise}` }</TextSimple>
            <TextSimple>{ `Telefone: ${phone}` }</TextSimple>
          </View>
        </TouchableNativeFeedback>
        <Row style={{ justifyContent: 'space-between' }}>
          <IconButton
            onPress={() => this.openSocial(facebook)}
            color={COLOR_FACEBOOK}
            icon="facebook"
          />
          <IconButton
            onPress={() => this.openSocial(linkedin)}
            color={COLOR_LINKEDIN}
            icon="linkedin"
          />
          <IconButton
            onPress={() => this.openSocial(twitter)}
            color={COLOR_TWITTER}
            icon="twitter"
          />
        </Row>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  imageStyle: {
    flex: 1,
    alignSelf: 'stretch',
    width: undefined,
    height: 150,
    resizeMode: 'cover',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    marginBottom: 5
  },
  itemContainerStyle: {
    marginTop: PADDING_CARD,
    backgroundColor: 'white',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  textCenter: {
    textAlign: 'center'
  },
  textJust: {
    textAlign: 'justify',
    marginTop: 5,
    marginBottom: 5
  },
  placeStyle: {
    alignContent: 'center'
  },
});

export default EnterpriseItem;
