import React from 'react';
import { View, StyleSheet } from 'react-native';

const Row = (props) => (
  <View style={[styles.container, props.style]} onLayout={props.onLayout}>
    { props.children }
  </View>
);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center'
  }
});

export { Row };
