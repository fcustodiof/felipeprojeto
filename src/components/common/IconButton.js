import React from 'react';
import { StyleSheet, TouchableNativeFeedback, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { COLOR_PRIMARY } from '../../style/colors';

const IconButton = (props) => (
  <TouchableNativeFeedback
    background={TouchableNativeFeedback.SelectableBackground()}
    onPress={props.onPress}
  >
    <View
      style={[props.style, styles.buttonStyle]}
    >
      <Icon
        name={props.icon}
        size={props.iconSize || 30}
        color={props.color || COLOR_PRIMARY}
      />
    </View>
  </TouchableNativeFeedback>
);

const styles = StyleSheet.create({
  textStyle: {
    textAlign: 'center'
  },
  buttonStyle: {
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    alignContent: 'center'
  },
});

export { IconButton };
