import React from 'react';
import { Text, StyleSheet } from 'react-native';
import { COLOR_PRIMARY_TEXT } from '../../style/colors';

const TextSimple = (props) => (
  <Text
    ellipsizeMode="tail"
    style={[styles.textStyle, props.style]}
  >
    { props.children }
  </Text>
);

const styles = StyleSheet.create({
  textStyle: {
    color: COLOR_PRIMARY_TEXT,
    fontFamily: 'Roboto',
  },
});

export { TextSimple };
