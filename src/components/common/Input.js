import React from 'react';
import { Text, View, StyleSheet, TextInput } from 'react-native';
import { COLOR_PRIMARY_TEXT, COLOR_PRIMARY } from '../../style/colors';

class Input extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOnFocus: false
    };
  }

  changeStyleOnFocus() {
    const { isOnFocus } = this.state;
    if (isOnFocus) {
      return { borderColor: COLOR_PRIMARY };
    }
    return { borderColor: COLOR_PRIMARY_TEXT };
  }

  render() {
    const {
      value,
      onChangeText,
      style,
      placeholder,
      keyboardType,
      returnKeyType,
      textContentType,
      secureTextEntry,
      autoCorrect
    } = this.props;
    return (
      <View style={[styles.container, style]}>
        <TextInput
          keyboardType={keyboardType}
          returnKeyType={returnKeyType}
          textContentType={textContentType}
          secureTextEntry={secureTextEntry}
          value={value}
          autoCorrect={autoCorrect}
          onFocus={() => this.setState({ isOnFocus: true })}
          onBlur={() => this.setState({ isOnFocus: false })}
          placeholder={placeholder}
          style={[styles.textInputStyle, this.changeStyleOnFocus()]}
          onChangeText={onChangeText}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    height: 80,
    alignContent: 'center',
  },
  textInputStyle: {
    height: 56,
    borderColor: COLOR_PRIMARY_TEXT,
    borderBottomWidth: 2
  }
});
export { Input };
