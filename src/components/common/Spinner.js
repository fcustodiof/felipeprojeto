import React from 'react';
import { View, ActivityIndicator } from 'react-native';

const Spinner = (props) => (
  <View style={[styles.spinnerStyle, props.style]}>
    <ActivityIndicator size={props.size || 'large'} color={props.color} />
  </View>
);

const styles = {
  spinnerStyle: {
    fles: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10
  }
};

export { Spinner };
