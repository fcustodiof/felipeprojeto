import React from 'react';
import { View, StyleSheet } from 'react-native';

const Card = (props) => (
  <View
    pointerEvents={props.pointerEvents}
    style={[styles.container, props.style]}
  >
    { props.children }
  </View>
);

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: 0,
    margin: 3,
    backgroundColor: '#fff',
    borderRadius: 2,
    elevation: 1,
    shadowColor: '#000000',
    shadowOpacity: 0.3,
    shadowRadius: 1,
    shadowOffset: {
      height: 1,
      width: 0.3,
    }
  }
});

export { Card };
