import React from 'react';
import { Text } from 'react-native';
import { COLOR_PRIMARY_TEXT } from '../../style/colors';

const TextTitle = (props) => {
  const {
    ellipsizeMode,
    style,
    children
  } = props;
  return (
    <Text
      ellipsizeMode={ellipsizeMode}
      style={[styles.textStyle, style]}
    >
      { children }
    </Text>
  );
};

const styles = {
  textStyle: {
    fontSize: 16,
    fontWeight: 'bold',
    fontFamily: 'Roboto',
    color: COLOR_PRIMARY_TEXT
  }
};

export { TextTitle };
