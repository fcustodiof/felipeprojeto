import React from 'react';
import {
  View,
  StyleSheet,
  TouchableNativeFeedback,
  Text
} from 'react-native';
import { COLOR_PRIMARY } from '../../style/colors';

const Button = (props) => {
  const {
    onPress,
    disabled,
    style,
    children
  } = props;

  return (
    <TouchableNativeFeedback
      disabled={disabled}
      style={style}
      onPress={onPress}
      background={TouchableNativeFeedback.SelectableBackground()}
    >
      <View style={[styles.buttonStyle, { opacity: disabled ? 0.65 : 1 }]}>
        <Text style={styles.textButtonStyle}>
          {children}
        </Text>
      </View>
    </TouchableNativeFeedback>
  );
};

const styles = StyleSheet.create({
  buttonStyle: {
    height: 50,
    backgroundColor: COLOR_PRIMARY,
    justifyContent: 'center'
  },
  textButtonStyle: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 16,
    fontWeight: '500',
    textTransform: 'uppercase'
  }
});

export { Button };
