/* eslint-disable camelcase */
import React, { Component } from 'react';
import {
  ScrollView,
  StyleSheet
} from 'react-native';
import { COLOR_PRIMARY } from '../style/colors';
import { PADDING_VIEW } from '../style/dimensions';
import EnterpriseItem from '../components/EnterpriseItem';
import { getDetalhesEmpresas } from '../server/requests';
import { Spinner } from '../components/common';

export default class EnterpriseDetails extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('enterprise').enterprise_name
  });

  constructor(props) {
    super(props);
    const { navigation } = this.props;
    this.state = {
      enterprise: navigation.getParam('enterprise'),
      loading: false
    };
  }

  componentDidMount() {
    const { enterprise } = this.state;
    this.setState({ loading: true });
    getDetalhesEmpresas(enterprise.id)
      .then((response) => {
        console.log(response);
        this.setState({
          enterprise: response.data.enterprise,
          loading: false
        });
      }).catch((err) => {
        this.setState({ loading: false });
      });
  }

  renderData() {
    const { loading, enterprise } = this.state;
    if (loading) {
      return (
        <Spinner color={COLOR_PRIMARY} />
      );
    }
    return (
      <EnterpriseItem
        disabled
        item={enterprise}
      />
    );
  }

  render() {
    return (
      <ScrollView
        contentContainerStyle={styles.scrollContainer}
        style={styles.container}
      >
        { this.renderData() }
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  scrollContainer: {
    padding: PADDING_VIEW
  }
});