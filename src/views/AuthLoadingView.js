import React, { Component } from 'react';
import { View, Text } from 'react-native';

export default class AuthLoadingView extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    navigation.navigate('Auth');
  }

  render() {
    return (
      <View>
        <Text> AuthLoadingView </Text>
      </View>
    );
  }
}
