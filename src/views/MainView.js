/* eslint-disable camelcase */
import React, { Component } from 'react';
import {
  View,
  StatusBar,
  StyleSheet,
  FlatList
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { COLOR_SECUNDARY, COLOR_PRIMARY } from '../style/colors';
import * as Enterprise from '../store/enterpriseContainer/actions';
import { getEmpresas } from '../server/requests';
import { PADDING_VIEW, PADDING_CARD } from '../style/dimensions';
import EnterpriseItem from '../components/EnterpriseItem';
import { Spinner, TextTitle } from '../components/common';

const NUM_LIST_ROWS = 6;

class MainView extends Component {
  static navigationOptions = {
    title: 'Início'
  };

  _isMounted = false;

  constructor(props) {
    super(props);
    this.state = {
      enterprisesList: [],
      loading: false
    };
  }

  componentDidMount() {
    StatusBar.setBackgroundColor(COLOR_SECUNDARY, true);
    StatusBar.setBarStyle('light-content');
    
    this._isMounted = true;
    
    this.setState({ loading: true });

    getEmpresas()
      .then((response) => {
        if (this._isMounted) {
          this.setState({
            enterprisesList: response.data.enterprises,
            loading: false
          });
        }
      }).catch((error) => {
        this.setState({ loading: false });
      });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  renderEnterpriseItem = ({ item }) => {
    const { navigate } = this.props.navigation;
    return (
      <EnterpriseItem
        onPress={() => navigate('EnterpriseDetails', { enterprise: item })}
        item={item}
      />
    );
  }

  renderList() {
    const { enterprisesList, loading } = this.state;
    if (loading) {
      return (
        <Spinner color={COLOR_PRIMARY} />
      );
    }
    if (enterprisesList.length > 0) {
      return (
        <FlatList
          initialNumToRender={NUM_LIST_ROWS}
          contentContainerStyle={styles.listContentStyle}
          data={enterprisesList}
          renderItem={this.renderEnterpriseItem.bind(this)}
          keyExtractor={(e) => e.id.toString()}
        />
      );
    }
    return (
      <TextTitle>Lista Vazia</TextTitle>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        { this.renderList() }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
  },
  listContentStyle: {
    marginStart: PADDING_VIEW,
    marginEnd: PADDING_VIEW,
    marginTop: PADDING_CARD
  },
});

const mapStateToProps = (state) => ({
  error: state.login.error,
  loading: state.login.loading
});

const mapDispatchToProps = (dispatch) => bindActionCreators(Enterprise, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MainView);
