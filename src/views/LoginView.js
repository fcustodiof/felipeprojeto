import React, { Component } from 'react';
import {
  View,
  Image,
  StyleSheet,
  Alert
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { PADDING_VIEW } from '../style/dimensions';
import {
  Button,
  Input,
  Spinner,
  TextTitle
} from '../components/common';
import * as Login from '../store/loginContainer/actions';
import { COLOR_PRIMARY, COLOR_ACCENT } from '../style/colors';

const LOGO_IMAGE = require('../assets/images/logo_ioasys.png');

class LoginView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: 'testeapple@ioasys.com.br',
      password: '12341234',
    };
  }

  doLogin = () => {
    const { email, password } = this.state;
    const { doLoginUser } = this.props;
    if (email && password) {
      doLoginUser({ email, password });
    } else {
      Alert.alert(
        'Atenção',
        'Login inválido',
        [
          { text: 'Fechar' },
        ],
        { cancelable: false },
      );
    }
  }

  renderLoginButton() {
    const { loading } = this.props;
    if (loading) {
      return (
        <Spinner color={COLOR_PRIMARY} />
      );
    }
    return (
      <Button
        onPress={this.doLogin}
        style={styles.buttonStyle}
      >
          Login
      </Button>
    );
  }

  renderErrorLogin() {
    const { error } = this.props;
    if (error) {
      return (
        <TextTitle style={styles.textError}>
          Erro ao fazer login
        </TextTitle>
      );
    }
  }

  render() {
    const { email, password } = this.state;
    return (
      <View style={styles.container}>
        <Image
          style={styles.logoStyle}
          source={LOGO_IMAGE}
        />
        { this.renderErrorLogin() }
        <View style={styles.loginStyle}>
          <Input
            keyboardType="email-address"
            textContentType="emailAddress"
            autoCorrect={false}
            value={email}
            onChangeText={(text) => this.setState({ email: text })}
            placeholder="E-mail"
          />
          <Input
            keyboardType="default"
            returnKeyType="done"
            textContentType="password"
            secureTextEntry
            value={password}
            autoCorrect={false}
            onChangeText={(text) => this.setState({ password: text })}
            placeholder="Senha"
          />
        </View>
        { this.renderLoginButton() }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
  },
  logoStyle: {
    margin: PADDING_VIEW,
    flex: 1,
    alignSelf: 'stretch',
    width: undefined,
    height: 150,
    resizeMode: 'contain'
  },
  loginStyle: {
    marginEnd: PADDING_VIEW,
    marginStart: PADDING_VIEW
  },
  textError: {
    textAlign: 'center',
    color: COLOR_ACCENT
  }
});

const mapStateToProps = (state) => ({
  error: state.login.error,
  loading: state.login.loading
});

const mapDispatchToProps = (dispatch) => bindActionCreators(Login, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(LoginView);
